import React from 'react';
import UserGreeting from './userGreeting.js';
import GuestGreeting from './guestGreeting.js';

class Greeting extends React.Component {
  constructor(props) {
    super(props);
    this.state = {isLoggedIn: false};
  }

  handleClick() {
    this.setState({isLoggedIn: !this.state.isLoggedIn });
  }

  render () {
    if (this.state.isLoggedIn) {
      return <div>
                <UserGreeting />
                <button onClick={(e) => this.handleClick(e)}>
                  Logout
                </button>
              </div>;
    }
    return <div>
              <GuestGreeting />
              <button onClick={(e) => this.handleClick(e)}>
                Login
              </button>
          </div>;
  }

}

export default Greeting;

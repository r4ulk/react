import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import Welcome from './welcome.js';
import Comment from './user/comment.js';
import Clock from './clock.js';
import Greeting from './login/greeting.js';
import NameForm from './nameform.js';

class Index extends React.Component {
  render() {
    return (
      <div className="divContent">
        <NameForm />
        <hr />
        <Greeting />
        <hr />
        <Clock date={new Date()} />
        <hr />
        <Comment
          date={comment.date}
          text={comment.text}
          author={comment.author} />
        <hr />
        <Welcome name={comment.author.name} />
      </div>
    );
  }
}
const comment = {
  date: new Date(),
  text: 'Learning React!',
  author: {
    name: 'Raul Klumpp',
    avatarUrl: 'https://avatar-cdn.atlassian.com/577d4dff1ba3ab2888b7714a79917704'
  }
};

ReactDOM.render(
  <Index />,
  document.getElementById('root')
);

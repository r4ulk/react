import React from 'react';
import Avatar from './avatar.js';

class UserInfo extends React.Component {
  render () {
    return <div className="UserInfo">
              <Avatar
                avatarUrl={this.props.avatarUrl}
                name={this.props.name} />
                <div className="UserInfo-name">{this.props.name}</div>
           </div>
  }
}

export default UserInfo;

import React from 'react';
import UserInfo from './userInfo.js';

class Comment extends React.Component {
  formatDate(date) {
    return date.toLocaleDateString();
  }

  render() {
    return <div className="Comment">
              <UserInfo
                  avatarUrl={this.props.author.avatarUrl}
                  name={this.props.author.name} />
              <div className="Comment-text">
                {this.props.text}
              </div>
              <div className="Comment-date">
                {this.formatDate(this.props.date)}
              </div>
           </div>;
  }
}

export default Comment;

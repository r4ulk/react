const env = process.env;

export const nodeEnv = env.NODE_ENV || 'development';

export const logStars = message => {
  console.log('********');
  console.log('message: ', message);
  console.log('********');
};

export default {
  mongodbUri: 'mongodb://localhost:27017/test',
  port: env.PORT || 8080,
  host: env.HOST || '0.0.0.0',
  get serverURL() {
    return `http://${this.host}:${this.port}`;
  }
};

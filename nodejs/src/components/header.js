import React from 'react';
import PropTypes from 'prop-types';

const color = Math.random() > 0.5 ? 'green' : 'red';

const Header = ({message}) => {
  return (
    <h2 style={{color}} className="Header text-center">
      {message}
    </h2>
  );
};

Header.propTypes = {
  message: PropTypes.string.isRequired
};

export default Header;

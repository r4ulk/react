import express from 'express';
import { MongoClient } from 'mongodb';
import assert from 'assert';
import config from '../config';

//import data from '../src/testData';

let mdb;
MongoClient.connect(config.mongodbUri, (err, client) => {
  assert.equal(null, err);
  mdb = client.db('test');
});

const router = express.Router();
// const contests = data.contests.reduce((obj, contest) => {
//   obj[contest.id] = contest;
//   return obj;
// }, {});

router.get('/contests', (req, res) => {
  let contests = {};
  mdb.collection('contests').find({})
      .project({
        id: 1,
        categoryName: 1,
        contestName: 1
      })
      .each((err, contest) => {
        assert.equal(null, err);

        if(!contest){
          res.send({ contests });
          return;
        }

        contests[contest.id] = contest;
      });
  // res.send({
  //   contests: contests
  // });
});

router.get('/names/:nameIds', (req, res) => {
  const nameIds = req.params.nameIds.split(',').map(Number);
  let names = {};
  mdb.collection('names').find({ id: { $in: nameIds }})
      .each((err, contest) => {
        assert.equal(null, err);

        if(!contest){
          res.send({ names });
          return;
        }

        names[contest.id] = contest;
      });
});

router.get('/contests/:contestId', (req, res) => {
  mdb.collection('contests')
      .findOne({id: Number(req.params.contestId)})
      .then(contest => res.send(contest))
      .catch(console.error)
  // let contest = contests[req.params.contestId];
  // contest.description = 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.';
  // res.send(contest);
});

export default router;

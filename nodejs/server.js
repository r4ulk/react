import sassMiddleware from 'node-sass-middleware';
import path from 'path';

// ------------------ EXPRESS MODULE -----------
import config from './config';
// import fs from 'fs';

import apiRouter from './api';

import express from 'express';
const server = express();

server.use(sassMiddleware({
  src: path.join(__dirname, 'sass'),
  dest: path.join(__dirname, 'public')
}));

server.set('view engine', 'ejs');

// Render Contests from Backend
import serverRender from './serverRender';

server.get(['/', '/contest/:contestId'], (req, res) => {
  //res.send('Hello express!\n');
  serverRender(req.params.contestId)
    .then(({ initialMarkup, initialData } ) => {
      res.render('index', {
        initialMarkup,
        initialData
      });
    })
    .catch(console.error)
});

server.use('/api', apiRouter);
server.use(express.static('public'));

// server.get('/about.html', (req, res) => {
//   fs.readFile('./about.html', (err, data) => {
//     res.send(data.toString());
//   });
// });

server.listen(config.port, config.host, () => {
  console.info('Express listening on port:', config.port, 'on host:', config.host);
});

// ------------------ IMPORT MODULE -----------

// import config, { nodeEnv, logStars } from './config';
//
// // const nodeEnv = require('./config').nodeEnv;
//
// console.log(config, nodeEnv);
//
// logStars('this is a function');

// ------------------ HTTP MODULES -----------

//import https from 'https';

// https.get('https://www.lynda.com', res => {
//   console.log('Response status code:', res.statusCode);
//
//   res.on('data', chunk => {
//     console.log(chunk.toString());
//   });
// });

// import http from 'http';
//
// const server = http.createServer((req, res) => {
//   res.write('Hello HTTP!\n');
//   setTimeout(() => {
//     res.write('I can stream!\n');
//     res.end();
//   }, 3000);
// });
//
// server.listen(config.port);

// server.on('request', (req, res) => {
//   res.write('Hello HTTP!\n');
//   setTimeout(() => {
//     res.write('I can stream!\n');
//     res.end();
//   }, 3000);
// });
